{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "9e038cfa",
   "metadata": {},
   "source": [
    "\n",
    "<a id='data-statistical-packages'></a>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4c911a5d",
   "metadata": {},
   "source": [
    "# Data and Statistics Packages"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9f41ccd2",
   "metadata": {},
   "source": [
    "## Contents\n",
    "\n",
    "- [Data and Statistics Packages](#Data-and-Statistics-Packages)  \n",
    "  - [Overview](#Overview)  \n",
    "  - [DataFrames](#DataFrames)  \n",
    "  - [Statistics and Econometrics](#Statistics-and-Econometrics)  "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d722c7f2",
   "metadata": {},
   "source": [
    "## Overview\n",
    "\n",
    "This lecture explores some of the key packages for working with data and doing statistics in Julia.\n",
    "\n",
    "In particular, we will examine the `DataFrame` object in detail (i.e., construction, manipulation, querying, visualization, and nuances like missing data).\n",
    "\n",
    "While Julia is not an ideal language for pure cookie-cutter statistical analysis, it has many useful packages to provide those tools as part of a more general solution.\n",
    "\n",
    "This list is not exhaustive, and others can be found in organizations such as [JuliaStats](https://github.com/JuliaStats), [JuliaData](https://github.com/JuliaData/), and  [QueryVerse](https://github.com/queryverse)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6aa99de7",
   "metadata": {
    "hide-output": false
   },
   "outputs": [],
   "source": [
    "using LinearAlgebra, Statistics\n",
    "using DataFrames, RDatasets, DataFramesMeta, CategoricalArrays, Query\n",
    "using GLM"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "75a4e03b",
   "metadata": {},
   "source": [
    "## DataFrames\n",
    "\n",
    "A useful package for working with data is [DataFrames.jl](https://github.com/JuliaStats/DataFrames.jl).\n",
    "\n",
    "The most important data type provided is a `DataFrame`, a two dimensional array for storing heterogeneous data.\n",
    "\n",
    "Although data can be heterogeneous within a `DataFrame`, the contents of the columns must be homogeneous\n",
    "(of the same type).\n",
    "\n",
    "This is analogous to a `data.frame` in R, a `DataFrame` in Pandas (Python) or, more loosely, a spreadsheet in Excel.\n",
    "\n",
    "There are a few different ways to create a DataFrame."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f263b99c",
   "metadata": {},
   "source": [
    "### Constructing and Accessing a DataFrame\n",
    "\n",
    "The first is to set up columns and construct a dataframe by assigning names"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "92e7216d",
   "metadata": {
    "hide-output": false
   },
   "outputs": [],
   "source": [
    "using DataFrames, RDatasets  # RDatasets provides good standard data examples from R\n",
    "\n",
    "# note use of missing\n",
    "commodities = [\"crude\", \"gas\", \"gold\", \"silver\"]\n",
    "last_price = [4.2, 11.3, 12.1, missing]\n",
    "df = DataFrame(commod = commodities, price = last_price)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "64366c18",
   "metadata": {},
   "source": [
    "Columns of the `DataFrame` can be accessed by name using `df.col`, as below"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "adf950f5",
   "metadata": {
    "hide-output": false
   },
   "outputs": [],
   "source": [
    "df.price"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2a886d04",
   "metadata": {},
   "source": [
    "Note that the type of this array has values `Union{Missing, Float64}` since it was created with a `missing` value."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bdfc5115",
   "metadata": {
    "hide-output": false
   },
   "outputs": [],
   "source": [
    "df.commod"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1bb12512",
   "metadata": {},
   "source": [
    "The `DataFrames.jl` package provides a number of methods for acting on `DataFrame`’s, such as `describe`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "795d6611",
   "metadata": {
    "hide-output": false
   },
   "outputs": [],
   "source": [
    "DataFrames.describe(df)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "515596cc",
   "metadata": {},
   "source": [
    "While often data will be generated all at once, or read from a file, you can add to a `DataFrame` by providing the key parameters."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6745ffad",
   "metadata": {
    "hide-output": false
   },
   "outputs": [],
   "source": [
    "nt = (commod = \"nickel\", price = 5.1)\n",
    "push!(df, nt)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "feca9321",
   "metadata": {},
   "source": [
    "Named tuples can also be used to construct a `DataFrame`, and have it properly deduce all types."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0bd9d628",
   "metadata": {
    "hide-output": false
   },
   "outputs": [],
   "source": [
    "nt = (t = 1, col1 = 3.0)\n",
    "df2 = DataFrame([nt])\n",
    "push!(df2, (t = 2, col1 = 4.0))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f2b9c559",
   "metadata": {},
   "source": [
    "In order to modify a column, access the mutating version by the symbol `df[!, :col]`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0436e206",
   "metadata": {
    "hide-output": false
   },
   "outputs": [],
   "source": [
    "df[!, :price]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "19fce5c4",
   "metadata": {},
   "source": [
    "Which allows modifications, like other mutating `!` functions in julia."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5aa4128f",
   "metadata": {
    "hide-output": false
   },
   "outputs": [],
   "source": [
    "df[!, :price] *= 2.0  # double prices"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3e52e9a7",
   "metadata": {},
   "source": [
    "As discussed in the next section, note that the [fundamental types](https://julia.quantecon.org/../getting_started_julia/fundamental_types.html#missing), is propagated, i.e. `missing * 2 === missing`."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8b7129d1",
   "metadata": {},
   "source": [
    "### Working with Missing\n",
    "\n",
    "As we discussed in [fundamental types](https://julia.quantecon.org/../getting_started_julia/fundamental_types.html#missing), the semantics of `missing` are that mathematical operations will not silently ignore it.\n",
    "\n",
    "In order to allow `missing` in a column, you can create/load the `DataFrame`\n",
    "from a source with `missing`’s, or call `allowmissing!` on a column."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b89ec4b5",
   "metadata": {
    "hide-output": false
   },
   "outputs": [],
   "source": [
    "allowmissing!(df2, :col1) # necessary to add in a for col1\n",
    "push!(df2, (t = 3, col1 = missing))\n",
    "push!(df2, (t = 4, col1 = 5.1))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b6343b94",
   "metadata": {},
   "source": [
    "We can see the propagation of `missing` to caller functions, as well as a way to efficiently calculate with non-missing data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "557c762a",
   "metadata": {
    "hide-output": false
   },
   "outputs": [],
   "source": [
    "@show mean(df2.col1)\n",
    "@show mean(skipmissing(df2.col1))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6148a65b",
   "metadata": {},
   "source": [
    "And to replace the `missing`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3257210f",
   "metadata": {
    "hide-output": false
   },
   "outputs": [],
   "source": [
    "df2.col1 .= coalesce.(df2.col1, 0.0) # replace all missing with 0.0"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7dd5b71c",
   "metadata": {},
   "source": [
    "### Manipulating and Transforming DataFrames\n",
    "\n",
    "One way to do an additional calculation with a `DataFrame` is to tuse the `@transform` macro from `DataFramesMeta.jl`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "39d38da5",
   "metadata": {
    "hide-output": false
   },
   "outputs": [],
   "source": [
    "using DataFramesMeta\n",
    "f(x) = x^2\n",
    "df2 = @transform(df2, :col2=f.(:col1))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ff523b97",
   "metadata": {},
   "source": [
    "### Categorical Data\n",
    "\n",
    "For data that is [categorical](https://juliadata.github.io/DataFrames.jl/stable/man/categorical/)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "854ad4c2",
   "metadata": {
    "hide-output": false
   },
   "outputs": [],
   "source": [
    "using CategoricalArrays\n",
    "id = [1, 2, 3, 4]\n",
    "y = [\"old\", \"young\", \"young\", \"old\"]\n",
    "y = CategoricalArray(y)\n",
    "df = DataFrame(id = id, y = y)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "249a1c18",
   "metadata": {
    "hide-output": false
   },
   "outputs": [],
   "source": [
    "levels(df.y)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "45539a16",
   "metadata": {},
   "source": [
    "### Visualization, Querying, and Plots\n",
    "\n",
    "The `DataFrame` (and similar types that fulfill a standard generic interface) can fit into a variety of packages.\n",
    "\n",
    "One set of them is the [QueryVerse](https://github.com/queryverse).\n",
    "\n",
    "**Note:** The QueryVerse, in the same spirit as R’s tidyverse, makes heavy use of the pipeline syntax `|>`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "99dd4f45",
   "metadata": {
    "hide-output": false
   },
   "outputs": [],
   "source": [
    "x = 3.0\n",
    "f(x) = x^2\n",
    "g(x) = log(x)\n",
    "\n",
    "@show g(f(x))\n",
    "@show x |> f |> g; # pipes nest function calls"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3e7a81d0",
   "metadata": {},
   "source": [
    "To give an example directly from the source of the LINQ inspired [Query.jl](http://www.queryverse.org/Query.jl/stable/)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "86a37e86",
   "metadata": {
    "hide-output": false
   },
   "outputs": [],
   "source": [
    "using Query\n",
    "\n",
    "df = DataFrame(name = [\"John\", \"Sally\", \"Kirk\"],\n",
    "               age = [23.0, 42.0, 59.0],\n",
    "               children = [3, 5, 2])\n",
    "\n",
    "x = @from i in df begin\n",
    "    @where i.age > 50\n",
    "    @select {i.name, i.children}\n",
    "    @collect DataFrame\n",
    "end"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7511f0ce",
   "metadata": {},
   "source": [
    "While it is possible to just use the `Plots.jl` library, there are other options for displaying tabular data – such as [VegaLite.jl](https://github.com/queryverse/VegaLite.jl)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0eb962d5",
   "metadata": {},
   "source": [
    "## Statistics and Econometrics\n",
    "\n",
    "While Julia is not intended as a replacement for R, Stata, and similar specialty languages, it has a growing number of packages aimed at statistics and econometrics.\n",
    "\n",
    "Many of the packages live in the [JuliaStats organization](https://github.com/JuliaStats/).\n",
    "\n",
    "A few to point out\n",
    "\n",
    "- [StatsBase](https://github.com/JuliaStats/StatsBase.jl) has basic statistical functions such as geometric and harmonic means, auto-correlations, robust statistics, etc.  \n",
    "- [StatsFuns](https://github.com/JuliaStats/StatsFuns.jl) has a variety of mathematical functions and constants such as pdf and cdf of many distributions, softmax, etc.  "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b85c0dd3",
   "metadata": {},
   "source": [
    "### General Linear Models\n",
    "\n",
    "To run linear regressions and similar statistics, use the [GLM](http://juliastats.github.io/GLM.jl/latest/) package."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8ff9bbe3",
   "metadata": {
    "hide-output": false
   },
   "outputs": [],
   "source": [
    "using GLM\n",
    "\n",
    "x = randn(100)\n",
    "y = 0.9 .* x + 0.5 * rand(100)\n",
    "df = DataFrame(x = x, y = y)\n",
    "ols = lm(@formula(y~x), df) # R-style notation"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c59a046e",
   "metadata": {},
   "source": [
    "To display the results in a useful tables for LaTeX and the REPL, use\n",
    "[RegressionTables](https://github.com/jmboehm/RegressionTables.jl/) for output\n",
    "similar to the Stata package esttab and the R package stargazer."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "37b1baa3",
   "metadata": {
    "hide-output": false
   },
   "outputs": [],
   "source": [
    "using RegressionTables\n",
    "regtable(ols)\n",
    "# regtable(ols,  renderSettings = latexOutput()) # for LaTex output"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "47aff8f0",
   "metadata": {},
   "source": [
    "### Fixed Effects\n",
    "\n",
    "While Julia may be overkill for estimating a simple linear regression,\n",
    "fixed-effects estimation with dummies for multiple variables are much more computationally intensive.\n",
    "\n",
    "For a 2-way fixed-effect, taking the example directly from the documentation using [cigarette consumption data](https://github.com/johnmyleswhite/RDatasets.jl/blob/master/doc/plm/rst/Cigar.rst)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "739ed613",
   "metadata": {
    "hide-output": false
   },
   "outputs": [],
   "source": [
    "using FixedEffectModels\n",
    "cigar = dataset(\"plm\", \"Cigar\")\n",
    "cigar.StateCategorical = categorical(cigar.State)\n",
    "cigar.YearCategorical = categorical(cigar.Year)\n",
    "fixedeffectresults = reg(cigar,\n",
    "                         @formula(Sales~NDI + fe(StateCategorical) +\n",
    "                                        fe(YearCategorical)),\n",
    "                         weights = :Pop, Vcov.cluster(:State))\n",
    "regtable(fixedeffectresults)"
   ]
  }
 ],
 "metadata": {
  "date": 1694412290.3702939,
  "filename": "data_statistical_packages.md",
  "kernelspec": {
   "display_name": "Julia",
   "language": "julia",
   "name": "julia-1.9"
  },
  "title": "Data and Statistics Packages"
 },
 "nbformat": 4,
 "nbformat_minor": 5
}